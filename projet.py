# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 21:48:54 2017

@author: abdessamad et Killian
"""
from PIL import Image 
import os
import tkinter as tk

chemin_image = None
image = None
quitter = 0
revenir_menu_principal = 0

#Fonction qui va chercher l'image et l'ouvrir ( Avec chemin d'accès absolu ou relatif )
def ouvrir_image():
    def essai():
        try:
            global chemin_image
            global image
            chemin_image = entree_chemin.get()
            print(chemin_image)
            image = Image.open(chemin_image)
            image.show() # Afficher l'image
        except FileNotFoundError:
            tk.Label(fen, text="Le chemin est incorrect").pack()

    global chemin_image
    fen = tk.Tk()
    fen.title("traitement image")
    lbl = tk.Label(fen, text="Entrez le chemin")
    lbl.pack()
    entree_chemin = tk.Entry(fen)
    entree_chemin.pack()
    tk.Button(fen, text="Valider",fg = "green", command = essai).pack(side=tk.LEFT)
    tk.Button(fen, text="Quitter",fg = "red", command = fen.destroy).pack(side=tk.LEFT)
    
    
    fen.mainloop()
#Fonction qui va afficher l'image
 
def afficher_image():  
    global image
    if image== None:
        print("Vous devez d'abord ouvrir l'image une première fois. ")
    else:
        image.show()
        stocker_data_image()
 
 
#Fonction qui va afficher les informations concernant l'image (mode colorimétrique, format de l'image, dimensions)
 
def afficher_infos_image():
    global chemin_image
    varA = *image.size,
    root = tk.Tk()
    root.title("traiment image")
    tk.Label(root, text="Propriétés de l'image : " + str(chemin_image)).pack()
    tk.Label(root, text="     Mode colorimétrique :" + str(image.mode)).pack()
    tk.Label(root, text="     Format de l'image :" + (image.format)).pack()
    tk.Label(root, text="     Dimensions : " + str(varA)).pack()
    tk.Button(root, text="Retour",fg = "red",  command = root.destroy).pack()
     
    
   
#Fonction qui va inverser la couleur pour chaque pixel ( Boucles ? // Créer la fonction et ne pas se baser sur celles déjà existante )
 
def negatif_couleur():
    (largeur, hauteur)= image.size
    for x in range(largeur):
                for y in range(hauteur):
                    (rouge,vert,bleu) = image.getpixel((x,y))
                    (rouge,vert,bleu) = (255-rouge,255-vert,255-bleu)
                    image.putpixel((x,y),(rouge,vert,bleu))
    image.show()
 
#Fonction qui va faire pivoter l'ensemble de l'image (RGB->RBG // RGB->BGR // RGB->GBR // RBG->GRB)
 
def rotation_couleurs():
    (largeur, hauteur)= image.size
    def rgb():
        image.show()
    def bgr():
        for x in range(largeur): #Parcourir la largeur de l'image
                for y in range(hauteur):#Parcourir la longueur de l'image
                    (rouge,vert,bleu) = image.getpixel((x,y)) #Récupérer les pixels (x,y) et les stocker dans une liste
                    (rouge,vert,bleu) = (bleu,vert,rouge)
                    image.putpixel((x,y),(rouge,vert,bleu))
        image.show()
    def grb():
        for x in range(largeur):
                for y in range(hauteur):
                    (rouge,vert,bleu) = image.getpixel((x,y))
                    (rouge,vert,bleu) = (vert,bleu,rouge)
                    image.putpixel((x,y),(rouge,vert,bleu))
        image.show()
    def gbr():
        for x in range(largeur):
                for y in range(hauteur):
                    (rouge,vert,bleu) = image.getpixel((x,y))
                    (rouge,vert,bleu) = (vert,bleu,rouge)
                    image.putpixel((x,y),(rouge,vert,bleu))
        image.show()
    root = tk.Tk()

    root.title("traiment image")
    tk.Label(root, text="Quel mode de rotation colorimétrique voulez-vous ?").pack()
    tk.Button(root, text="       - RGB - Rotation RGB->RBG", command = rgb).pack()
    tk.Button(root, text="       - BGR - Rotation RGB->BGR", command = bgr).pack()
    tk.Button(root, text="       - GBR - Rotation RGB->GBR", command = grb).pack()
    tk.Button(root, text="       - GRB - Rotation RGB->GRB", command = grb).pack()
    tk.Button(root, text="Retour",fg = "red",  command = root.destroy).pack()


    
#Fonction qui va faire pivoter l'ensemble de l'image (-90°,90° et 180°)
 
def rotation_image():
    def anglea():
        def positif():
            global image
            """PROBLEME"""
            image2 = image
            fen = tk.Tk()
            fen.title("traiment image")
            lbl = tk.Label(fen, text="Entrez le nom du fichier")
            lbl.pack()
            input_utilisateur = tk.Entry(fen)
            input_utilisateur.pack()
            bouton_valider= tk.Button(fen, text="Valider",fg = "green", command = save)
            bouton_valider.pack()


        angle = 90
        image2 = image.rotate(angle)
        image2.show()



        
    def angledroitinverse():
        def negatif():
            def essai():
               nom = noms.get()
               image2.save(nom) 
            global image
            image2 = image
            fen = tk.Tk()
            fen.title("traiment image")
            lbl = tk.Label(fen, text="Entrez le nom du fichier")
            lbl.pack()
            noms = tk.Entry(root)
            noms.pack()
            nomss= tk.Button(fen, text="Valider",fg = "green", command = essai)
            nomss.pack()
            
        angle = -90
        image2 = image.rotate(angle)
        image2.show()
        fen = tk.Tk()
        fen.title("traiment image")
        tk.Label(fen, text="Voulez vous sauvegarder cette image ?").pack()
        tk.Button(fen, text="Oui", command = negatif).pack()

        
    def angleretourne():
        def positif():
            def essai():
                nom = noms.get()
                image2.save(nom) #problème
            global image
            image2 = image
            fen = tk.Tk()
            fen.title("traiment image")
            tk.Label(fen, text="Entrez le nom du fichier").pack()
            noms = tk.Entry(fen)
            nomss= tk.Button(fen, text="Valider",fg = "green", command = essai)
            nomss.pack()
            nom = noms.get()
            image2.save(nom)
            fen.mainloop()
            
        angle = 180
        image2 = image.rotate(angle)
        image2.show
        fen = tk.Tk()
        fen.title("traiment image")
        tk.Label(fen, text="Voulez vous sauvegarder cette image ?").pack()
        tk.Button(fen, text="Oui", command = positif).pack()
        fen.mainloop()
        
    def angle_choix():
        def rotation():
            global image
            angle2 = angleentre.get()
            float(angle2)
            image = image.rotate(angle2).show()
        def positif():           
        
        
        global image
        fen2 = tk.Tk()
        fen2.title("traiment image")
        tk.Label(fen2, text="Donnez l'angle").pack()
        angleentre = tk.Entry(fen2)
        angleentre.pack()
        tk.Button(fen2, text="Valider",fg = "green", command = rotation).pack()
        fen2.mainloop()
        
            
    root = tk.Tk()
    root.title("traiment image")
    tk.Label(root, text="Quelle diretion de rotation voulez-vous ?")
    
    tk.Button(root, text="Vers la droite", command = angledroit).pack()
    
    tk.Button(root, text="Vers la gauche", command = angledroitinverse).pack()
    
    tk.Button(root, text="Retourner", command = angleretourne).pack()
    
    tk.Button(root, text="Choisir", command = angle_choix).pack()
    
    tk.Button(root, text="Retour",fg = "red",  command = root.destroy).pack()

    root.mainloop()
        

 
# Fonction qui permet de définir la direction de rotation physique (Gauche, droite, retourner b )	
def symetrie_image():
    def symetrieV():
        SymétrieV = image.transpose(Image.FLIP_LEFT_RIGHT)
        SymétrieV.show()
    def symetrieH():
        SymétrieH = image.transpose(Image.FLIP_TOP_BOTTOM)
        SymétrieH.show()
    root = tk.Tk()
    tk.Label(root, text="Selon quel axe voulez-vous effectuer la symétrie ?").pack()
    frame=tk.Frame(root)
    frame.pack()
    root.title("symétrie image")
    tk.Button(frame, text="Symétrie verticale (ou symétrique d'axe horizontal", command = symetrieH).pack()
    tk.Button(frame, text="Symétrie Horizontale (ou symétrique d'axe vertical", command = symetrieV).pack()
    tk.Button(frame, text="Retour",fg = "red", command = root.destroy).pack()


    root.mainloop()
#Fonction qui nettoie l'écran ( supprimer tout élements présent sur l'écran = détruire ) 
 
def nettoyer_ecran():
    os.system("cls")
 
# Fonction qui arrête le programme(Message d'arrêt)
 
def quitter_programme():
    print("Fin du programme, au revoir.")
    exit
def save():
    def essai():
        nom_image = input_utilisateur.get()
        image2.save(nom_image) #problème
        tk.Label(fen, text="Votre sauvegarde s'est effectuée avec succès !", fg = "green").pack()
        tk.Label(fen, text="Cliquer sur la croix en haut à doite pour fermer cette fenêtre", fg = "green").pack()
    global image
    image2 = image
    fen = tk.Tk()
    fen.title("Traiment image")
    label = tk.Label(fen, text="Entrez le nom du fichier")
    label.pack()
    input_utilisateur = tk.Entry(fen)
    input_utilisateur.pack()
    bouton_valider= tk.Button(fen, text="Valider",fg = "green", command = essai)
    bouton_valider.pack()
    
def stocker_data_image():
    
    
    # Open a file
    
    
    pixel_image = list(image.getdata())
    data = open("pixel_image.txt", "w")
    data.write( "{}".format(pixel_image))
    data.close()
 
# Fonction qui crée le menu et le structure (O,A,I,N,C,R,S,Q)
 
def menu():
    root = tk.Tk()
    root.title("traiment image")
    tk.Button(root, text="Ouvrir l'image et l'afficher", command = ouvrir_image).pack()
    tk.Button(root, text="Afficher l'image précédement chargée", command = afficher_image).pack()
    tk.Button(root, text="Afficher les informations concernant l'image", command = afficher_infos_image).pack()
    tk.Button(root, text="Mettre l'image en négatif ", command = negatif_couleur).pack()
    tk.Button(root, text="Effectuer la rotation des couleurs de l'image", command = rotation_couleurs).pack()
    tk.Button(root, text="Effectuer la rotation physique de l'image ", command = rotation_image).pack()
    tk.Button(root, text="Effectuer une transformation symétrique de l'image", command = symetrie_image).pack()
    tk.Button(root, text="Effacer la console ", command = nettoyer_ecran).pack()
    tk.Button(root, text= "Sauvegarder l'image", command = save).pack()
    tk.Button(root, text="Quitter le programme", fg = "red", command = root.destroy).pack()
    root.mainloop()
    
if __name__ == "__main__":
    menu()
