# -*- coding: utf-8 -*-
"""
Created on Fri Nov 24 21:48:54 2017

@author: abdessamad et Killian
"""
from PIL import Image,ImageTk 
"""La bibliothèque   Python Imaging Library  (PIL) nous permet 
de manière facile d'effectuerdiverses traitements sur des images 
grâce à des fonctions ultérieurement codées"""
import os
"""Ce module est utile car il nous permet d'effacer la console"""
import tkinter as tk
"""La bibliothèque tkinter est une sorte de boite à outils 
qui nous permet de créer des interfaces graphiques"""


#Cette variable nous sert pour stocker le chemin qui nous permet d'accéder à l'image
chemin_image = None
#Cette variable Nous permet de stocker l'objet image initiale à partir de laquelle on va pouvoir faire 
image = None
# Cette variable permet de stocker une copie de l'objet image initialement chargée. On effectuera pas de modifications sur cette copie
image_modele = None
# Cette variable nous permet de stocker l'image une image compatible avec python
photo = None
# une variable qui va stocker la fonction qui nous mets de créer des fenêtres des tkinters
root = None
# Variable qui va stocker les images modifiées nous permettons ainsi de pouvoir 
image_modifie = None


def ouvrir_image():
    """
    - Fonction qui va chercher l'image et l'ouvrir ( Avec chemin d'accès absolu ou relatif ) 
    
    """
    def ouvrir_image_traitement():
        try: # On test ce morceau de code et en cas d'erreur [except]
            global chemin_image
            global image
            global image_modele
            # On récupère l'entrée de l'utilisateur (Chemin vers l'image)
            chemin_image = entree_chemin.get()
            print(chemin_image)
             # On ouvre l'image
            image = Image.open(chemin_image)
             # On copie l'image initiale et on la stock dans cette variable
            image_modele = image.copy()
            # Afficher l'image
            montrer_image(image) 

        #On exécute le code présent dans le except
        except FileNotFoundError: 
             #On utilise la classe Label qui prend plusieurs paramètres (parent,option)
            w = tk.Label(root, text="Le chemin est incorrect")
            #La méthode pack permet de positionner un objet dans une fenêtre ou dans un cadre
            w.pack()
    global chemin_image
     # On crée une fenêtre
    root = tk.Tk()
    # On demande à l'utilisateur de saisir le chemin vers l'image
    lbl = tk.Label(root, text="Entrez le chemin")
    lbl.pack()
    entree_chemin = tk.Entry(root) 
    entree_chemin.pack()
    #Btn pour délencher la fonction ouvrir_image()
    V = tk.Button(root, text="Valider",fg = "green", command = ouvrir_image_traitement)
    V.pack(side=tk.LEFT)
    #Btn pour quitter
    D = tk.Button(root, text="Quitter",fg = "red", command = root.destroy) 
    D.pack(side=tk.RIGHT)



def afficher_image():  
    """Fonction qui va permettre d'afficher l'image initiale autant de fois que l'on veut"""
    global image_modele 
    if image_modele == None: 
        fen = tk.Tk()
        fen.title("Traitement de l'image")
        tk.Label(fen, text="Un problème est survenu, nous ne pouvons pas ouvrir cette image !").pack()
    else:
        image_modele.show()


def montrer_image(image):
    """Fonction qui affiche les images dans la fenêtre ouverte par l'outil Tkinter"""

    photo = ImageTk.PhotoImage(image)   # Création d'une image compatible Tkinter
    label = tk.Label(image = photo)    # Insertion de l'image dans un label
    label.image = photo    #Maintenir l'image l'image en vie        
    label.pack() 

 
 

 
def afficher_infos_image():
    """Fonction qui va afficher les informations concernant l'image (mode colorimétrique, format de l'image, dimensions)"""
    global chemin_image
    taille_image = *image.size, # "* = Tous" (On a deux paramètres : Largeur x Hauteur)
    root = tk.Tk()
    root.title("Traitement de l'image")
    tk.Label(root, text="Propriétés de l'image : " + str(chemin_image)).pack()
    tk.Label(root, text="     Mode colorimétrique :" + str(image.mode)).pack()
    tk.Label(root, text="     Format de l'image :" + (image.format)).pack()
    tk.Label(root, text="     Dimensions : " + str(taille_image)).pack()
    tk.Button(root, text="Retour",fg = "red",  command = root.destroy).pack()
 
#Fonction qui va inverser la couleur pour chaque pixel 
 
def negatif_couleur():
    global image_modifie
    #On souhaite effectuer les modification sur l'image modèle et non sur des images déjà modifiées
    image_negatif = image_modele.copy() 
    # On récupère la taille dans une liste
    (largeur, hauteur) = image.size 
    for x in range(largeur):
            for y in range(hauteur):
                #On récupère les pixels présents dans la ligne X et colonne Y, pour chaque pixel on lis son  3-tuple
                    (rouge,vert,bleu) = image_negatif.getpixel((x,y))
                    # On applique l'effet négatif
                    (rouge,vert,bleu) = (255-rouge,255-vert,255-bleu)
                    #On crée le pixel finale 
                    image_negatif.putpixel((x,y),(rouge,vert,bleu)) 
    image_negatif.show()
    #Nécéssaire pour la sauvegarde
    image_modifie = image_negatif.copy() 
 

def rotation_couleurs():
    """Fonction qui va faire pivoter l'ensemble de l'image (RGB->RBG // RGB->BGR // RGB->GBR // RBG->GRB)"""
 
    global image_modele
    #On récupère la taille dans une liste
    (largeur, hauteur)= image.size 
    def rgb():
        global image_modifie
        image_modele.show()
        image_modifie = image_modele.copy()
    def bgr():
        global image_modifie
        image_bgr = image_modele.copy()
        #Parcourir la largeur de l'image
        for x in range(largeur): 
            #Parcourir la longueur de l'image
                for y in range(hauteur):
                    #Récupérer les pixels (x,y) et les stocker dans une liste
                    (rouge,vert,bleu) = image_bgr.getpixel((x,y)) 
                    #On effectue la rotation des couleurs (change la position de chaque pixel)
                    (rouge,vert,bleu) = (bleu,vert,rouge)
                    #On crée le pixel final
                    image_bgr.putpixel((x,y),(rouge,vert,bleu))
        image_bgr.show()
        image_modifie = image_bgr.copy()
    def brg():
        global image_modifie
        image_brg = image_modele.copy()
        for x in range(largeur):
                for y in range(hauteur):
                    (rouge,vert,bleu) = image_brg.getpixel((x,y))
                    (rouge,vert,bleu) = (bleu,rouge,vert)
                    image_brg.putpixel((x,y),(rouge,vert,bleu))
        image_brg.show()
        image_modifie = image_brg.copy()
    def gbr():
        global image_modifie
        image_gbr = image_modele.copy()
        for x in range(largeur):
                for y in range(hauteur):
                    (rouge,vert,bleu) = image_gbr.getpixel((x,y))
                    (rouge,vert,bleu) = (vert,bleu,rouge)
                    image_gbr.putpixel((x,y),(rouge,vert,bleu))
        image_gbr.show()
        image_modifie = image_gbr.copy()
    root = tk.Tk()

    root.title("Traitement de l'image")
    tk.Label(root, text="Quel mode de rotation colorimétrique voulez-vous ?").pack()
    tk.Button(root, text="       - RBG - Rotation RGB->RGB", command = rgb).pack()
    tk.Button(root, text="       - BGR - Rotation RGB->BGR", command = bgr).pack()
    tk.Button(root, text="       - GRB - Rotation RGB->BRG", command = brg).pack()
    tk.Button(root, text="       - GBR - Rotation RGB->GBR", command = gbr).pack()
    tk.Button(root, text="Retour",fg = "red",  command = root.destroy).pack()
    

 
def rotation_image():
    """Fonction qui va faire pivoter l'ensemble de l'image (-90°,90° et 180° et choix)"""
    global image
    image_modifie = image_modele.copy()
    def plus90():
        image = image_modele.copy()
        global image_modifie           
        angle = 90
        #ça permet de faire un filtrage Bicubique et une adatptation de l'image
        image_modifie = image.rotate(angle, Image.BICUBIC, True) 
        image_modifie.show()
        #on stock dans une variable la fonction qui va nous permettre de créer une fenêtre avec Tkinter
        fen = tk.Tk()
        #On affiche le titre
        fen.title("Traitement de l'image")
        tk.Label(fen, text="Voulez vous sauvegarder cette image ?").pack()
        tk.Button(fen, text="Oui", command = save).pack()
        
    def moins90():
        image = image_modele.copy()
        global image_modifie          
        angle = -90
        image_modifie = image.rotate(angle, Image.BICUBIC, True)
        image_modifie.show()
        fen = tk.Tk()
        fen.title("Traitement de l'image")
        tk.Label(fen, text="Voulez vous sauvegarder cette image ?").pack()
        tk.Button(fen, text="Oui",fg="blue", command = save).pack()

        
    def plus180():
        image = image_modele.copy()
        global image_modifie
        angle = 180
        image_modifie = image.rotate(angle, Image.BICUBIC, True)
        image_modifie.show()
        fen = tk.Tk()
        fen.title("Traitement de l'image")
        tk.Label(fen, text="Voulez vous sauvegarder cette image ?").pack()
        tk.Button(fen, text="Oui",fg="blue", command = save).pack()
        
    def angle_choix():
        image = image_modele.copy()
        
        def angle_choix_traitement():
            global image_modifie
            global image
            global angle_image
            angle_image = int(str(angle_image.get()))
            angle_adapte = angle_image % 360.0
            image_angle_choix = image.rotate(angle_adapte, Image.BICUBIC, True)
            image_angle_choix.show()
            image_modifie = image_angle_choix.copy()
            
        global angle_image
        global image_modifie
        fen_angle_choix = tk.Tk()
        fen_angle_choix.title("Traitement de l'image")
        tk.Label(fen_angle_choix, text="Donnez l'angle").pack()
        angle_image = tk.Entry(fen_angle_choix)
        angle_image.pack()
        tk.Button(fen_angle_choix, text="Valider",fg = "green", command = angle_choix_traitement).pack()
        fen_angle_choix.mainloop()
            
            
    root = tk.Tk()
    root.title("Traitement de l'image")
    tk.Label(root, text="Quelle diretion de rotation voulez-vous ?")
    
    tk.Button(root, text="Vers la droite", command = moins90).pack()
    
    tk.Button(root, text="Vers la gauche", command = plus90).pack()
    
    tk.Button(root, text="Retourner", command = plus180).pack()
    
    tk.Button(root, text="Choisir", command = angle_choix).pack()
    
    tk.Button(root, text="Retour",fg = "red",  command = root.destroy).pack()

    root.mainloop()
 
	
def symetrie_image():     
    """Fonction qui permet de définir la direction de rotation physique (Gauche, droite, retourner b )"""      
    global image
    def symetrieV():
        global image_modifie
        image = image_modele.copy()
        # On utilise une fonction qui permet de faire la symétrie Verticale
        SymetrieV = image.transpose(Image.FLIP_LEFT_RIGHT)
        SymetrieV.show()
        image_modifie = SymetrieV.copy()
        
    def symetrieH():
        global image_modifie
        image = image_modele.copy()
        # On utilise une fonction qui permet de faire la symétrie Horizontale
        SymetrieH = image.transpose(Image.FLIP_TOP_BOTTOM)
        SymetrieH.show()
        image_modifie = SymetrieH.copy()
    root = tk.Tk()
    tk.Label(root, text="Selon quel axe voulez-vous effectuer la symétrie ?").pack()
    root.title("Symétrie image")
    tk.Button(root, text="Symétrie verticale (ou symétrique d'axe horizontal", command = symetrieH).pack()
    tk.Button(root, text="Symétrie Horizontale (ou symétrique d'axe vertical", command = symetrieV).pack()
    tk.Button(root, text="Retour",fg = "red", command = root.destroy).pack()
    root.mainloop()
    
def save():
    """Fonction permet de sauvegarder les images """
    def sauvegarde():
        #On récupère l'entrée de l'utilisateur, ici le nom de l'image
        nom_image = input_utilisateur.get()
        #On sauvegarde l'image grâce à la fonction save
        image_save.save(nom_image) 
        #On affiche un message de succès de l'opération et propose à l'utilisateur de quitter la fenêtre
        tk.Label(fen, text="Votre sauvegarde s'est effectuée avec succès !", fg = "green").pack()
        tk.Button(fen, text="Quitter", fg = "red", command = fen.destroy).pack()
    global image
    image_save = image_modifie.copy()
    fen = tk.Tk()
    fen.title("Traitement de l'image")
    label = tk.Label(fen, text="Entrez le nom du fichier")
    label.pack()
    input_utilisateur = tk.Entry(fen)
    input_utilisateur.pack()
    bouton_valider= tk.Button(fen, text="Valider",fg = "green", command = sauvegarde)
    bouton_valider.pack()
    
def nettoyer_ecran():
    """Fonction qui permet de nottoyer le terminal"""
    os.system("cls")
 
 
def quitter_programme():
    """Fonction qui arrête le programme(Message d'arrêt)"""
    global root
    root.destroy()
    print("Fin du programme, au revoir.")
    exit
 
def menu():
    """Fonction qui crée le menu et le structure (O,A,I,N,C,R,S,Q)"""
    global root
    #Création de la fenêtre principale
    root = tk.Tk()
    #On lui donne un titre
    root.title("Traitement de l'image")
    #Ensemble de boutons permettant de créer un menu et donnant accès aux diverses fonctionnalités 
    tk.Button(root, text="Ouvrir l'image et l'afficher", command = ouvrir_image).pack()
    tk.Button(root, text="Afficher l'image précédement chargée", command = afficher_image).pack()
    tk.Button(root, text="Afficher les informations concernant l'image", command = afficher_infos_image).pack()
    tk.Button(root, text="Mettre l'image en négatif ", command = negatif_couleur).pack()
    tk.Button(root, text="Effectuer la rotation des couleurs de l'image", command = rotation_couleurs).pack()
    tk.Button(root, text="Effectuer la rotation physique de l'image ", command = rotation_image).pack()
    tk.Button(root, text="Effectuer une transformation symétrique de l'image", command = symetrie_image).pack()
    tk.Button(root, text="Effacer la console ", command = nettoyer_ecran).pack()
    tk.Button(root, text= "Sauvegarder l'image", command = save).pack()
    tk.Button(root, text="Quitter le programme", fg = "red", command = quitter_programme).pack()
    #Fonction qui maintient le menu en vie grâce à une boucle (On peut l'arrêter en détruisant le menu)
    root.mainloop()
        

if __name__ == "__main__":
    menu()

